import './style.css';
import 'antd/dist/antd.css';

import React from "react";
import { Checkbox, Divider } from 'antd';

const ColorSquare = ({ color }: { color: string }) => (
  <svg height="15" width="20">
    <rect x="5" y="0" width="15" height="15" fill={color}/>
  </svg>
);

export interface OptionsProps {
  visible: Set<string>;
  setVisible: (visible: Set<string>) => void;
  news: any[];
}

const onChangeCheckbox = (e: any, visible: Set<string>, setVisible: (visible: Set<string>) => void, name: string) => {
  const newVisible = new Set(visible);
  if (e.target.checked) {
    newVisible.add(name);
  } else {
    newVisible.delete(name);
  }
  setVisible(newVisible);
};

const Options: React.FC<OptionsProps> = ({ visible, setVisible, news }) => {
  return (
    <div className="options">
      <Checkbox className="options_items" onChange={e => onChangeCheckbox(e, visible, setVisible, 'Intel')}>
        <span className="options_name">Intel <ColorSquare color="#8884d8"/></span>
      </Checkbox>
      <br/>
      <Checkbox className="options_items" onChange={e => onChangeCheckbox(e, visible, setVisible, 'BitCoin')}>
        <span className="options_name">BitCoin <ColorSquare color="#82ca9d"/></span>
      </Checkbox>
      <br/>
      <Checkbox className="options_items" onChange={e => onChangeCheckbox(e, visible, setVisible, 'Tesla')}>
        <span className="options_name">Tesla <ColorSquare color="#9c3214"/></span>
      </Checkbox>
      <Divider className="options_devider" />
      <div>
        {news.map(e => (
          <div className="news_item">
            <div className="name"><a href={e.source}>{e.title}</a></div>
            <div>{e.description}</div>
            <img src={e.image}  className="photo" />
          </div>
        ))}
      </div>
    </div>
  );
};

export default Options;