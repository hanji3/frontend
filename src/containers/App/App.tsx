import React, {useEffect, useState} from 'react';
import './App.css';
import Visualization from "../Visualization";
import Options from "../Options";

const SUPPORTED_CURRENCIES: string[] = ['Intel', 'BitCoin', 'Tesla'];

let socket: WebSocket | null = null;

const initWs = (setData: any, setNews: any) => {
  if (socket != null) {
    return;
  }

  socket = new WebSocket('ws://localhost:8080/chat/info');

  socket.addEventListener('open', function (event) {
    console.log('ws connection established');
  });

  socket.addEventListener('message', function (event) {
    console.log(event.data);
    try {
      let data = JSON.parse(event.data);
      console.log('parsed:');
      console.log(data);

      if (data instanceof Array) {
        for (const stock of data) {
          if (stock.hasOwnProperty("description")) {
            setNews({ ...stock });
          } else {
            setData({ name: stock.name, timestamp: stock.timestamp, ...stock.values });
          }
        }
      } else {
        if (data.hasOwnProperty("description")) {
          setNews({ ...data });
        } else {
          setData({ name: data.name, timestamp: data.timestamp, ...data.values });
        }
      }
    } catch (e) {
      console.log(e);
    }
  });

  socket.addEventListener('close', function (event) {
    console.log('socket closed');
    socket = null;
  });
};

const filterData = (data: any[], visible: Set<string>): any[] => {
  if (visible.size === 0) {
    return data;
  }
  return data.map(e => {
    let res = { ...e };
    for (const currency of SUPPORTED_CURRENCIES) {
      if (!visible.has(currency)) {
        delete res[currency];
      }
    }
    return res;
  });
}

const App: React.FC = ({}) => {
  let [data, setData] = useState<any[]>([]);
  let [newMessage, setNewMessage] = useState<any>(null);

  let [visible, setVisible] = useState<Set<string>>(new Set());

  let [news, setNews] = useState<any[]>([]);
  let [newNews, setNewNews] = useState<any>(null);

  useEffect(() => {
    console.log('init ws');
    initWs((newData: any) => {
      setNewMessage(newData);
    }, (newNews: any) => {
      setNewNews(newNews);
    });
  }, [data]);

  useEffect(() => {
    setData([...data, newMessage].slice(-20));
  }, [newMessage]);

  useEffect(() => {
    console.log('init ws');
    initWs((newData: any) => {
      setNewMessage(newData);
    }, (newNews: any) => {
      setNewNews(newNews);
    });
  }, [news]);

  useEffect(() => {
    setData([...news, newNews]);
  }, [newNews]);

  return (
    <div className="App">
      <Visualization data={filterData(data, visible)} />
      <Options setVisible={setVisible} visible={visible} news={news}/>
    </div>
  );
}

export default App;