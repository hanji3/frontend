import React from "react";
import { Area, AreaChart, CartesianGrid, Tooltip, XAxis, YAxis } from "recharts";

import './style.css';
import 'antd/dist/antd.css';

export interface VisualizationProps {
  data: any[];
}

const Visualization: React.FC<VisualizationProps> = ({ data }) => {
  return (
    <div className="visualization">
      <AreaChart width={930} height={500} data={data}
                 margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
        <defs>
          <linearGradient id="colorBitCoin" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
            <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
          </linearGradient>
          <linearGradient id="colorDogeCoin" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8}/>
            <stop offset="95%" stopColor="#82ca9d" stopOpacity={0}/>
          </linearGradient>
          <linearGradient id="colorEthereum" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#9c3214" stopOpacity={0.8}/>
            <stop offset="95%" stopColor="#9c3214" stopOpacity={0}/>
          </linearGradient>
        </defs>
        <XAxis dataKey="name" />
        <YAxis />
        <CartesianGrid strokeDasharray="3 3" />
        <Tooltip />
        <Area type="monotone" dataKey="Intel" stroke="#8884d8" fillOpacity={1} fill="url(#colorBitCoin)" isAnimationActive={false} />
        <Area type="monotone" dataKey="BitCoin" stroke="#82ca9d" fillOpacity={1} fill="url(#colorDogeCoin)" isAnimationActive={false} />
        <Area type="monotone" dataKey="Tesla" stroke="#9c3214" fillOpacity={1} fill="url(#colorEthereum)" isAnimationActive={false} />
      </AreaChart>
    </div>
  );
};

export default Visualization;